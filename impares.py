try:
    n1 = int(input("Dame un número entero no negativo:"))
    n2 = int(input("Ahora dame otro:"))
    neg = 0

    if n1 < 0:
        print("n1 ha de ser un numero positivo")
        exit()
    if n2 < 0:
        print("n2 ha de ser un numero positivo")
        exit()
    else:
        for x in range(n1, n2 + 1) or range(n2, n1 + 1):
            if x % 2 != 0:
                neg += x
                print(f"{x}, ", end="")
                ##print(", ", end= "")
        print("")
        print(neg)
except:
    print("Has de poner numeros enteros no negativos.")
